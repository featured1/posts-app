import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import vuetify from "./plugins/vuetify";
import setupVeeValidate from "./plugins/vee.validate";

Vue.config.productionTip = false;

setupVeeValidate();

new Vue({
  vuetify,
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
