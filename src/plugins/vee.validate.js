import Vue from "vue";
import * as rules from "vee-validate/dist/rules";
import { extend, ValidationObserver, ValidationProvider } from "vee-validate";

export default function setupVeeValidate() {
  Vue.component("ValidationProvider", ValidationProvider);
  Vue.component("ValidationObserver", ValidationObserver);

  for (const [rule, validation] of Object.entries(rules)) {
    extend(rule, {
      ...validation,
    });
  }
}
