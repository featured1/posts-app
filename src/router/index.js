import Vue from "vue";
import VueRouter from "vue-router";
import PostPage from "../pages/PostPage";
import PostsIndexPage from "../pages/PostsIndexPage";
import PostCommentsPage from "../pages/PostCommentsPage";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Posts",
    component: PostsIndexPage,
  },
  {
    path: "/post/:id",
    name: "Post Page",
    component: PostPage,
  },
  {
    path: "/post/:id/comments",
    name: "Post Comments Page",
    component: PostCommentsPage,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
