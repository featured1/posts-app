import Vue from "vue";
import Vuex from "vuex";
import appModule from "./modules/app";
import postsModule from "./modules/posts/index";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    app: appModule,
    posts: postsModule,
  },
});
