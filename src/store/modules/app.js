const initialState = () => ({
  validationErrors: {},
  alertMessage: {
    display: false,
    caption: "",
    type: "green",
  },
  confirmationModal: {
    opened: false,
    accepted: false,
    action: "",
    title: "",
    caption: "",
    item: {},
  },
});
const appModule = {
  namespaced: true,
  state: initialState(),
  actions: {
    showAlert({ commit }, payload) {
      commit("ALERT_SHOWED", {
        display: true,
        ...payload,
      });
    },
    hideAlert({ commit }) {
      commit("ALERT_HIDDEN");
    },
    openConfirmation({ commit }, payload) {
      commit("CONFIRMATION_RESET");
      commit("CONFIRMATION_OPENED", {
        opened: true,
        ...payload,
      });
    },
    closeConfirmation({ commit }, payload) {
      commit("CONFIRMATION_ACCEPTANCE", payload.accepted);
      commit("CONFIRMATION_CLOSED");
    },
    setValidationErrors({ commit }, errors) {
      commit("SET_VALIDATION_ERRORS", errors);
    },
  },
  mutations: {
    ALERT_SHOWED(state, payload) {
      state.alertMessage = { ...payload };
    },
    ALERT_HIDDEN(state) {
      state.alertMessage = { ...initialState().alertMessage };
    },
    CONFIRMATION_RESET(state) {
      state.confirmationModal = { ...initialState().confirmationModal };
    },
    CONFIRMATION_OPENED(state, payload) {
      state.confirmationModal = { ...payload };
    },
    CONFIRMATION_ACCEPTANCE(state, accepted) {
      state.confirmationModal.accepted = accepted;
    },
    CONFIRMATION_CLOSED(state) {
      state.confirmationModal = { ...initialState().confirmationModal };
    },
    SET_VALIDATION_ERRORS(state, errors) {
      state.validationErrors = errors;
    },
  },
  getters: {},
};

export default appModule;
