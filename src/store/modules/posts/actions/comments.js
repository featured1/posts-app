export default {
  async saveComment({ state, commit }) {
    const comment = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${state.editPost.id}/comments`,
      {
        method: "POST",
        body: JSON.stringify({
          ...state.editComment.data,
          userId: 1,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      }
    )
      .then((response) => response.json())
      .then((json) => json);

    commit("ADD_COMMENT_TO_POST", comment);
    this.dispatch("app/showAlert", {
      type: "green",
      caption: "Your comment was posted successfully",
    });
  },
  async editComment({ commit }, comment) {
    commit("SET_COMMENT_DIALOG_STATUS", open);
    commit("SET_EDIT_COMMENT", {
      id: comment.id,
      data: {
        name: comment.name,
        email: comment.email,
        body: comment.body,
      },
    });
  },
  updateComment({ commit }) {
    commit("UPDATE_POST_COMMENT");
    commit("SET_COMMENT_DIALOG_STATUS", false);
    this.dispatch("app/showAlert", {
      type: "green",
      caption: "The comment was updated successfully",
    });
  },
  async deleteComment({ state, commit }, commentId) {
    try {
      await fetch(
        `https://jsonplaceholder.typicode.com/posts/${state.editPost.id}/comments/${commentId}`,
        {
          method: "DELETE",
        }
      );

      commit("REMOVE_COMMENT_FROM_POST", commentId);

      this.dispatch("app/showAlert", {
        type: "green",
        caption: "The comment was deleted successfully",
      });
    } catch (err) {
      this.dispatch("app/showAlert", {
        type: "red",
        caption: `There was an error deleting the comment ${err}`,
      });
    }
  },
  setCommentDialogStatus({ commit }, status) {
    commit("SET_COMMENT_DIALOG_STATUS", status);
  },
  resetEditComment({ commit }) {
    commit("RESET_EDIT_COMMENT");
  },
};
