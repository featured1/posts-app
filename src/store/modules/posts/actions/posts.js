export default {
  async getAll(store) {
    let posts = await fetch("https://jsonplaceholder.typicode.com/posts")
      .then((response) => response.json())
      .then((json) => json);

    posts = posts.map((post) => ({ ...post, comments: [] }));

    store.commit("SET_POSTS", posts);
  },
  async getPostById({ commit }, postId) {
    try {
      const post = await fetch(
        `https://jsonplaceholder.typicode.com/posts/${postId}`
      )
        .then((response) => response.json())
        .then((json) => json);

      if (!post) {
        return;
      }

      const postComments = await fetch(
        `https://jsonplaceholder.typicode.com/posts/${postId}/comments`
      )
        .then((response) => response.json())
        .then((json) => json);

      post.comments = [...postComments];
      commit("SET_EDIT_POST", post);

      return true;
    } catch (err) {
      return false;
    }
  },
};
