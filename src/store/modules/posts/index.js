import postActions from "./actions/posts";
import postMutations from "./mutations/posts";
import commentsActions from "./actions/comments";
import commentsMutations from "./mutations/comments";
import { getField, updateField } from "vuex-map-fields";

export const initialState = () => ({
  list: [],
  commentDialogIsOpened: false,
  editComment: {
    id: "",
    data: {
      name: "",
      email: "",
      body: "",
    },
  },
  editPost: {
    id: "",
    userId: "",
    title: "",
    body: "",
    comments: [],
  },
});

const postsModule = {
  namespaced: true,
  state: initialState(),
  actions: {
    ...postActions,
    ...commentsActions,
  },
  mutations: {
    ...postMutations,
    ...commentsMutations,
    updateField,
  },
  getters: {
    getField,
  },
};

export default postsModule;
