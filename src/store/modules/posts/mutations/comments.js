import Vue from "vue";
import { initialState } from "../index";

export default {
  ADD_COMMENT_TO_POST(state, comment) {
    const commentsCopy = state.editPost.comments.slice(0);
    commentsCopy.unshift(comment);
    state.editPost.comments = [...commentsCopy];
  },
  UPDATE_POST_COMMENT(state) {
    const updatedComment = {
      id: state.editComment.id,
      ...state.editComment.data,
    };

    const commentIndex = state.editPost.comments.findIndex(
      (comment) => comment.id === updatedComment.id
    );

    if (commentIndex !== -1) {
      Vue.set(state.editPost.comments, commentIndex, updatedComment);
    }
  },
  REMOVE_COMMENT_FROM_POST(state, commentId) {
    const commentsCopy = state.editPost.comments.slice(0);

    const commentIndex = commentsCopy.findIndex(
      (comment) => comment.id === commentId
    );

    if (commentIndex != -1) {
      commentsCopy.splice(commentIndex, 1);
      state.editPost.comments.splice(0);
      state.editPost.comments = [...commentsCopy];
    }
  },
  SET_COMMENT_DIALOG_STATUS(state, status) {
    state.commentDialogIsOpened = status;
  },
  SET_EDIT_COMMENT(state, payload) {
    state.editComment = {
      id: payload.id,
      data: { ...payload.data },
    };
  },
  RESET_EDIT_COMMENT(state) {
    state.editComment = { ...initialState().editComment };
  },
};
