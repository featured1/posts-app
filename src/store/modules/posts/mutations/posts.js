export default {
  SET_POSTS(state, posts) {
    state.list.splice(0);
    state.list = [...posts];
  },
  SET_EDIT_POST(state, post) {
    state.editPost = { ...post };
  },
};
